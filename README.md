## Project status
Uploaded project template - Added a final scenario for testing proper
brick recycling, good luck!

## Cellular Automata Retro Voxel Fluids

## Description
This project implements a Cellular automata based fluid simulation added onto the Retro Voxel Template https://github.com/jbikker/WrldTmpl8, this
version has been provided for the INFOMOV course as an optional project for optimization. The relevant code can be found in the WaterWorld project,
specifically the CAPE class (cape.h, cape.cpp), which implements the simulation. The scenario is initialised in WaterWorld.cpp. It is particle-free
simulation, where the state of the cells is directly transformed to voxels in the world.

##How Does it Work?
The simulation follows along the lines of a traditional Eulerian fluid simulation, trying to solve or approximate the navier-stokes equations, but
does so only locally and restricts movement of material to adjacent cells only. In this cellular automata style approach, values in the grid are updated
as a function of values in its direct neighbourhood, this is also known as stencil computing.

In order to save space and determine what cells need to be visited, the world is split into bricks of customizable size (currently 4x4x4, but any power of 2 can be chosen). A brick only exists if it is considered relevant to the simulation, it must either contain water, or be adjacent to a cell that does so. The simulation only 
processes bricks that are considered alive, and will automatically allocate new bricks where required as the water flows into new areas.

The simulation pipeline is as follows:

1. Create and destroy new bricks to store simulation data as is required by the current state of the simulation
2. Perform material advection: update the material in each cell as a function of it's neighbours and velocities
3. Perform velocity self advection: self advect the momentum and determine new velocities
4. Determine divergence: Determine and store the divergence: the amount that the water violates the mass constraints
5. Solve pressure: Iteratively determine a pressure value that will allow us to correct the velocities to be mass conserving
6. Apply this correction, and finalize velocities by enforcing some constraints.

##Configuring the simulation
An initial simulation scenario is provided and set up in waterworld.cpp. It is recommended that you benchmark against this initial scenario.
Parameters of the simulation can be tweaked in cape.h, although changing them is not really recommended. Some things to keep in mind:

Try to keep water walled in, if you allow water to spread across a large plane it may quickly spread very thin and create many bricks that need
to be simulated, without actually having much total volume.

When using a MAXV setting of 1.0f, the simulation is vulnerable to negative mass violations, make sure the update rate is low enough for the specific
scenario at play, or use MAXV 0.33f or below to be guaranteed mass conservation.

Using higher Pressure iterations gives better results, but is quite costly, leave at ~8 for now, but specifically optimizing the pressure solver,
allowing more iterations is therefore a good goal.

##FAQ
For questions send an email to w.g.vanderscheer@students.uu.nl, or contact me via microsoft teams via the same email.